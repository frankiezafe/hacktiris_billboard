# all global vars for the billboard

import os
import datetime
import enum
import tabler

### PATHS ###

# storage
FOLDER_HISTORY = 'storage/history'
FOLDER_CACHE = 'storage/cache'
FOLDER_STAGING = 'storage/staging'
FOLDER_HOTFOLDER = 'storage/hotfolder'
# templates
TEMPLATE_PATH_REGISTRATION = 'tmpl/registration.txt'
TEMPLATE_PATH_WELCOME = 'tmpl/welcome.txt'
TEMPLATE_PATH_EVENT = 'tmpl/event.txt'
TEMPLATE_PATH_EVENT_VALIDATION = 'tmpl/event_validation.txt'
TEMPLATE_PATH_INFO = 'tmpl/info.txt'
TEMPLATE_PATH_NOTIF = 'tmpl/notification.txt'
TEMPLATE_PATH_MESSAGE = 'tmpl/message.txt'

### SECURITY & DEBUG ###

LOG_MAIL = 'storage/mailer.log'
CONTROL_WHITE_LIST = 'storage/whitelist'
CONTROL_BLACK_LIST = 'storage/blacklist'
CONTROL_ROLES = 'storage/roles'
CONTROL_AUTO_EDITOR = False
class Role(enum.IntEnum):
	ERROR = 0
	REJECT = 1
	UNKNOWN = 2
	EDITOR = 10
	ADMIN = 100

### FORMS ###

BB_EMAIL_UNIQUE = 'hbb_UID'
BB_EMAIL_FROM = 'hbb_FROM'
BB_EMAIL_REPLY = 'hbb_REPLY'
BB_EMAIL_ROLE = 'hbb_ACCESS'

SUBJECT_FLAG_REGISTRATION = 'Hacktiris Billboard system (HBB)'
SUBJECT_FLAG_WELCOME = 'Hacktiris Billboard system (HBB)'
SUBJECT_FLAG_EVENT = 'HBB_EVENT:'
SUBJECT_FLAG_INFO = 'HBB_INFO:'
SUBJECT_FLAG_NOTIF = 'HBB_NOTIF:'
SUBJECT_FLAG_USER = 'HBB_USER:'
SUBJECT_FLAG_TEMPLATE = 'TEMPLATE'
SUBJECT_FLAG_SUBMISSION = 'SUBMISSION'
SUBJECT_FLAG_VALIDATION = 'VALIDATION'
SUBJECT_FLAG_CONFIRM = 'CONFIRMATION'
SUBJECT_FLAG_END = ''
SUBJECT_FLAG_UID = 'UID:'
SUBJECT_FLAG_EVENT_NAME = 'EVENT_NAME:'

# administration
SUBJECT_FLAG_BLACKLIST = ['blacklist']
SUBJECT_FLAG_ADMIN = ['admin']

TEMPLATE_TITLE = 'Hacktiris billboard'
TEMPLATE_FOOTER = 'the hacktiris billboard system, developped by frankie@frankiezafe.org'

# forms
FIELDS_EVENT = [
	{
		'name': SUBJECT_FLAG_EVENT_NAME,
		'limit': 75,
		'type': 'TXT',
		'mandatory': True
	},
	{
		'name': 'EVENT_DESCRIPTION:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'EVENT_START_DATE:',
		'limit': -1,
		'type': 'DATE',
		'mandatory': True
	},
	{
		'name': 'EVENT_END_DATE:',
		'limit': -1,
		'type': 'DATE',
		'mandatory': False
	},
	{
		'name': 'EVENT_START_HOUR:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'EVENT_END_HOUR:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'EVENT_PRICE:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'EVENT_LEVEL:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'EVENT_ROOM:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'EVENT_CONTACT:',
		'limit': 20,
		'type': 'TXT',
		'mandatory': True
	}
]

FIELDS_INFO = [
	{
		'name': 'INFO_GROUP_OR_COMPANY:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'INFO_YOUR_NAME:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': True
	},
	{
		'name': 'INFO_LEVEL:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': False
	},
	{
		'name': 'INFO_ROOM:',
		'limit': -1,
		'type': 'DATE',
		'mandatory': False
	},
	{
		'name': 'INFO_CONTACT:',
		'limit': -1,
		'type': 'TXT',
		'mandatory': True
	}
]

FIELDS_NOTIF = [
	{
		'name': 'NOTIF_MESSAGE:',
		'limit': 100,
		'type': 'TXT',
		'mandatory': True
	}
]

FIELDS_VALIDATION = [
	{
		'name': SUBJECT_FLAG_UID,
		'limit': 100,
		'type': 'TXT',
		'mandatory': True
	}
]

# messages
MESSAGE_EVENT_SUB_ERROR = "!Your submission is not valid, please complete all mandatory fields and resend!\n\n"
MESSAGE_EVENT_VAL_OK = "Your event is validated! It will appear shortly in the billboard!\n"
MESSAGE_EVENT_VAL_ERROR = "Your event nas not been validated... Please re-submit it or contact billboard administrators.\n"

### LAYOUT ###

LAYOUT_DATE_FORMAT = "%d/%m/%Y"

LAYOUT_EVENT_PREVIEW = tabler.tabler_config()
LAYOUT_EVENT_PREVIEW['canvas'] = [80,200]
LAYOUT_EVENT_PREVIEW['display_rect'][2] = LAYOUT_EVENT_PREVIEW['canvas'][0]
LAYOUT_EVENT_PREVIEW['display_rect'][3] = LAYOUT_EVENT_PREVIEW['canvas'][1]
LAYOUT_EVENT_PREVIEW['cell_padding'] = [0,1,0,1]
LAYOUT_EVENT_PREVIEW['cell_border'] = 1
LAYOUT_EVENT_PREVIEW['table_columns'] = 1
LAYOUT_EVENT_PREVIEW['dump'] = False
LAYOUT_EVENT_PREVIEW['cell_style_enabled'] = False
LAYOUT_EVENT_PREVIEW['word_wrap'] = 'word'

LAYOUT_EVENT_PREVIEW_DESC = tabler.cell_config( LAYOUT_EVENT_PREVIEW )
LAYOUT_EVENT_PREVIEW_DESC['cell_maxh'] = 5

LAYOUT_EVENT_PREVIEW_CONTACT = tabler.cell_config( LAYOUT_EVENT_PREVIEW )
LAYOUT_EVENT_PREVIEW_CONTACT['cell_maxh'] = 2

### UTILS ###


def check_folder( path, do_create = True ):
	out = True
	if not os.path.isdir( path ):
		if do_create:
			os.makedirs( path )
		out = False
	return out

def check_file( path ):
	d = os.path.dirname( os.path.abspath(path) )
	check_folder( d )

def log( path, msg ):
	check_file( path )
	f = open( path, 'a' )
	f.write( str(datetime.datetime.now().timestamp()) + '::' + msg + '\n' )
	f.close()