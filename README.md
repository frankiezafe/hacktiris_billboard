```bash
################################################################################
#         ______  __            ______ __________       _____                  #
#         ___  / / /_____ _________  /___  /___(_)_________(_)_______          #
#         __  /_/ /_  __ `/  ___/_  //_/  __/_  /__  ___/_  /__  ___/          #
#         _  __  / / /_/ // /__ _  ,<  / /_ _  / _  /   _  / _(__  )           #
#         /_/ /_/  \__,_/ \___/ /_/|_| \__/ /_/  /_/    /_/  /____/            #
#                                                                              #
#         _____________________________                     _________          #
#         ___  __ )__(_)__  /__  /__  /_____________ _____________  /          #
#         __  __  |_  /__  /__  /__  __ \  __ \  __ `/_  ___/  __  /           #
#         _  /_/ /_  / _  / _  / _  /_/ / /_/ / /_/ /_  /   / /_/ /            #
#         /_____/ /_/  /_/  /_/  /_.___/\____/\__,_/ /_/    \__,_/             #
#                                                                              #
#                                                                              #
################################################################################
                                                                                
python suite to create billboards with multi pages, timer, animation and image  
transformation into ascii displayable in terminal                               
                                                                                
////////////////////////////////////////////////////////////////////////////////
/                                                   ,                          /
/                    ._. _  _.. .*._. _ ._ _  _ ._ -+- __                      /
/                    [  (/,(_](_||[  (/,[ | )(/,[ ) | _)                       /
/                            |                                                 /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
                                                                                
/ python:                                                                       
                                                                                
    $ pip install pyfiglet                                                      
    $ pip install termcolor                                                     
    $ pip install Pillow                                                        
    $ pip install numpy                                                         
    $ pip install dateparser                                                    
                                                                                
/ third-party:                                                                  
                                                                                
    $ sudo apt install ffmpeg                                                   
                                                                                
                                                                                
////////////////////////////////////////////////////////////////////////////////
/                                                                              /
/                               . . __ _. _  _                                 /
/                               (_|_) (_](_](/,                                /
/                                        ._|                                   /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
                                                                                
/ python files in this repository                                               
                                                                                
*file*                              *description*                     *status*  
                                                                                
gifer.py         convert animated gifs into text sequence, requires      ready  
                 ffmpeg & imager.py                                             
                                                                                
imager.py        convert a single image into text                        ready  
                                                                                
mailer.py        mail server, receives and sends mails to update         ready  
                 the billboard                                                  
                                                                                
layouter.py      turns mail content into displayable text               in dev  
                                                                                
pager.py         management of pages                                    in dev  
                                                                                
pyfligeter.py    print all fonts avalable in filget, sorted by font      ready  
                 name                                                           
                                                                                
readme.py        used to generate this readme                            ready  
                                                                                
tabler.py        generation of tables and management of styles           ready  
                                                                                
                                                                                
/ how to use tabler.py?                                                         
                                                                                
tabler_config(c)    generates a standard configuration of a table, with         
                    styles enabled and 4 columns                                
                    returns a deepcopy of the passed configuration if some      
                                                                                
quick_table(t,c)    generates a table with one column                           
                    - arg[0]: text to display                                   
                    - arg[1]: configuration                                     
                                                                                
tabler(t,c)         generates a table with multiple columns and rows            
                    - arg[0]: list of cells/text to display                     
                    - arg[1]: configuration                                     
                                                                                
                                                                                
/ table configuration                                                           
                                                                                
*general                                                                        
                                                                                
canvas              [ width, height ], the canvas to draw the table, set        
                    automatically to the terminal window dimensions             
                    unit: character                                             
                                                                                
display_rect        [ x, y width, height ] visible part of the canvas, set      
                    automatically to [ 0, 0, canvas[0], canvas[1] ]             
                    unit: character                                             
                                                                                
dump                if true, print the table directly in the terminal           
                                                                                
export_path         path of a file to dump the table content, does not          
                    create folders                                              
                                                                                
packing             describes the way to put the lines in the table             
                    - strict_H: horizontal packing, rows are created first      
                    and all cells from the same row have the same height        
                    - strict_V: vertical packing, columns are created first     
                    then stacked and all cells from the same row have the       
                    same height                                                 
                    - fluid_H: horizontal packing, rows are created first       
                    and cells placed in the next smaller column                 
                    - fluid_V: vertical packing, columns are created first      
                    and avoid reaching the canvas height, might generates       
                    new columns                                                 
                    see below for packing demo                                  
                                                                                
table_columns       number of columns of the table                              
                    strictly applied by all packing except 'fluid_V'            
                                                                                
return              type of return of tabler() function                         
                    - cropped: content ready to print                           
                    - lines: each lines of the table, not cropped with          
                    display_rect                                                
                    - cells: all cells of the table                             
                    - size: disable content processing, just compute the        
                    dimensions of the table, to be retrieved directly in the    
                    config (usefull when using nested tables)                   
                    - none: nothing                                             
                                                                                
word_wrap           word wrapping method                                        
                    - 'letter': by default, line returns anywhere in the the    
                    text, ensure an optimised cell size                         
                    - 'word', tries to only generate line returns between       
                    word                                                        
                    - 'none': truncates long lines (usefull for images!)        
                                                                                
*styling                                                                        
                                                                                
border_char         character to be used for borders, '·' by default            
                                                                                
border_color        decoration of the border, blank by default                  
                                                                                
cell_border         thickness of the borders                                    
                                                                                
cell_maxh           maximum cell height, set to -1 to disable                   
                                                                                
cell_padding        [ top, right, bottom, left ] padding of each cell           
                                                                                
cell_style_enabled  if true, special characters will be added to set            
                    decoration                                                  
                                                                                
cell_style_even     decoration of the even cells, '\033[40m\033[1;37m' by       
                    default, white text on black bg                             
                                                                                
cell_style_odd      decoration of the odd cells, '\033[47m\033[1;31m' by        
                    default, red text on grey bg                                
                                                                                
empty_char          character to be used for empty spaces, ' ' by default       
                                                                                
padding_char        character to be used for padding, ' ' by default            
                                                                                
truncated_marker    when cell_maxh is enabled and content is too long, this     
                    marker will be appended at the end of the text, '[...]'     
                    by default                                                  
                                                                                
vertical_align      vertical alignment of text in cell, 'left', 'center' or     
                    'right'                                                     
                                                                                
style_reset         command used to reset the default decoration, '\033[m'      
                    by default                                                  
                                                                                
*processed                                                                      
                                                                                
table_width         width of the table without the borders, automatically       
                    computed                                                    
                                                                                
cell_outer          width of each column, including padding but without         
                    borders                                                     
                                                                                
cell_offset         horizontal offset of each column, without borders           
                                                                                
                                                                                
/ cell configuration                                                            
                                                                                
*general                                                                        
                                                                                
word_wrap           overwrites the table configuration, 'letter' (default),     
                    'word' or 'none'                                            
                                                                                
*styling                                                                        
                                                                                
cell_padding        [ top, right, bottom, left ] padding of this cell           
                                                                                
cell_maxh           maximum cell height, set to -1 to disable                   
                                                                                
vertical_align      vertical alignment of text in cell, 'left', 'center' or     
                    'right'                                                     
                                                                                
*processed                                                                      
                                                                                
cell_inner          width of the cell without borders nor padding               
                                                                                
                                                                                
/ packing demo : strict_H                                                       
                                                                                
··············································································· 
·                         ·                         ·                         · 
· [1] Pellentesque        · [2] python suite to     · [3] https://twoglassham · 
· habitant morbi          · create billboards       · s.itch.io/ashi-wash-cla · 
· tristique senectus et   · with multi pages,       · ssic                    · 
· netus et malesuada      · timer, animation and    ·                         · 
· fames ac turpis         · image transformation    ·                         · 
· egestas. Vestibulum     · into ascii              ·                         · 
· et consectetur massa,   ·                         ·                         · 
· placerat pretium        ·                         ·                         · 
· turpis                  ·                         ·                         · 
·                         ·                         ·                         · 
··············································································· 
·                         ·                         ·                           
· [4] render form each    · [5]                     ·                           
· side in png,            ·     - https://robotvaul ·                           
· orthogonal projection   · t.bitbucket.io/scenenet ·                           
· relocate the origin     · -rgbd.html              ·                           
·                         ·     - https://carlburto ·                           
·                         · n.itch.io/islands       ·                           
·                         ·                         ·                           
·····················································                           
                                                                                
/ packing demo : strict_V                                                       
                                                                                
··············································································· 
·                         ·                         ·                         · 
· [1] Pellentesque        · [3] https://twoglassham · [5]                     · 
· habitant morbi          · s.itch.io/ashi-wash-cla ·     - https://robotvaul · 
· tristique senectus et   · ssic                    · t.bitbucket.io/scenenet · 
· netus et malesuada      ·                         · -rgbd.html              · 
· fames ac turpis         ·                         ·     - https://carlburto · 
· egestas. Vestibulum     ·                         · n.itch.io/islands       · 
· et consectetur massa,   ·                         ·                         · 
· placerat pretium        ·                         ·                         · 
· turpis                  ·                         ·                         · 
·                         ·                         ·                         · 
··············································································· 
·                         ·                         ·                           
· [2] python suite to     · [4] render form each    ·                           
· create billboards       · side in png,            ·                           
· with multi pages,       · orthogonal projection   ·                           
· timer, animation and    · relocate the origin     ·                           
· image transformation    ·                         ·                           
· into ascii              ·                         ·                           
·                         ·                         ·                           
·····················································                           
                                                                                
/ packing demo : fluid_H                                                        
                                                                                
··············································································· 
·                         ·                         ·                         · 
· [1] Pellentesque        · [2] python suite to     · [3] https://twoglassham · 
· habitant morbi          · create billboards       · s.itch.io/ashi-wash-cla · 
· tristique senectus et   · with multi pages,       · ssic                    · 
· netus et malesuada      · timer, animation and    ·                         · 
· fames ac turpis         · image transformation    ··························· 
· egestas. Vestibulum     · into ascii              ·                         · 
· et consectetur massa,   ·                         · [4] render form each    · 
· placerat pretium        ··························· side in png,            · 
· turpis                  ·                         · orthogonal projection   · 
·                         · [5]                     · relocate the origin     · 
···························     - https://robotvaul ·                         · 
                          · t.bitbucket.io/scenenet ··························· 
                          · -rgbd.html              ·                           
                          ·     - https://carlburto ·                           
                          · n.itch.io/islands       ·                           
                          ·                         ·                           
                          ···························                           
                                                                                
/ packing demo : fluid_V                                                        
                                                                                
···············································································
·                         ·                         ·                         ·
· [1] Pellentesque        · [2] python suite to     · [4] render form each    ·
· habitant morbi          · create billboards       · side in png,            ·
· tristique senectus et   · with multi pages,       · orthogonal projection   ·
· netus et malesuada      · timer, animation and    · relocate the origin     ·
· fames ac turpis         · image transformation    ·                         ·
· egestas. Vestibulum     · into ascii              ···························
· et consectetur massa,   ·                         ·                         ·
· placerat pretium        ··························· [5]                     ·
· turpis                  ·                         ·     - https://robotvaul ·
·                         · [3] https://twoglassham · t.bitbucket.io/scenenet ·
··························· s.itch.io/ashi-wash-cla · -rgbd.html              ·
                          · ssic                    ·     - https://carlburto ·
                          ·                         · n.itch.io/islands       ·
                          ···························                         ·
                                                    ···························
////////////////////////////////////////////////////////////////////////////////
/                                                                              /
/                         ._. _  __ _ . .._. _. _  __                          /
/                         [  (/,_) (_)(_|[  (_.(/,_)                           /
/                                                                              /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
                                                                                
                                                                                
image manipulation                                                              
                                                                                
- image to ascii:                                                               
https://www.geeksforgeeks.org/converting-image-ascii-image-python/              
- image resize:                                                                 
https://duckduckgo.com/?q=python+pil+resize+image&t=lm&ia=web&iax=qa            
                                                                                
gif resource:                                                                   
                                                                                
- goat/0001:                                                                    
https://i.pinimg.com/originals/be/93/99/be93997cc655c66b88d9c2ad50a1d658.gif    
                                                                                
                                                                                
```