import os
import pyfiglet
import termcolor

rows, cols = os.popen('stty size', 'r').read().split()
rows = int( rows )
cols = int( cols )

h = ''.join( [ '*' for i in range( cols ) ] )

f = pyfiglet.Figlet()
fonts = f.getFonts()
fonts.sort()
for f in fonts:
	fi = pyfiglet.Figlet(font=f, width=cols)
	print( h )
	print( '**\t' + f )
	print( fi.renderText('hacktiris billboard') )
	print( h )