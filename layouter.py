import os
import dateparser

import billboard_globals as bbglob
import imager
import gifer
import tabler

def event_content( fields ):
	
	lines = ['' for i in range(0,6)]
	for f in fields:
		if len(f['content']) == 0:
			continue
		if f['name'] == bbglob.SUBJECT_FLAG_EVENT_NAME:
			lines[0] = f['content']
		elif f['name'] == 'EVENT_DESCRIPTION:':
			lines[1] = f['content']
		elif f['name'] == 'EVENT_START_DATE:':
			lines[2] = str( dateparser.parse(f['content']).strftime( bbglob.LAYOUT_DATE_FORMAT ) )
		elif f['name'] == 'EVENT_END_DATE:':
			if len(lines[2]) != 0:
				lines[2] += ' > '
			lines[2] += str( dateparser.parse(f['content']).strftime( bbglob.LAYOUT_DATE_FORMAT ) )
		elif f['name'] == 'EVENT_START_HOUR:':
			if len(lines[2]) != 0:
				lines[2] += ', '
			lines[2] += f['content']
		elif f['name'] == 'EVENT_END_HOUR:':
			if len(lines[2]) != 0:
				lines[2] += ' > '
			lines[2] += f['content']
		elif f['name'] == 'EVENT_LEVEL:':
			lines[3] += 'level: ' + f['content']
		elif f['name'] == 'EVENT_ROOM:':
			if len( lines[3] ) > 0:
				lines[3] += ', '
			lines[3] += 'room ' + f['content']
		elif f['name'] == 'EVENT_PRICE:':
			lines[4] += 'price:' + f['content']
		elif f['name'] == 'EVENT_CONTACT:':
			lines[5] += 'contact:' + f['content']
	
	if len( lines[1] ) > 0:
		lines[1] = {
			'content': lines[1],
			'cfg': bbglob.LAYOUT_EVENT_PREVIEW_DESC
		}
	if len( lines[5] ) > 0:
		lines[5] = {
			'content': lines[5],
			'cfg': bbglob.LAYOUT_EVENT_PREVIEW_CONTACT
		}
	
	# removal of empty lines
	tmp = list( lines )
	lines = []
	for l in tmp:
		if not isinstance(l,str) or ( isinstance(l,str) and len( l ) != 0 ):
			lines.append( l )
	
	return lines

# called by mailer, requires fields to be loaded with data
def generate_preview( fields, subject_flag ):
	
	if subject_flag == bbglob.SUBJECT_FLAG_EVENT:
		
		# generating an event preview
		return tabler.tabler( event_content( fields ), bbglob.LAYOUT_EVENT_PREVIEW )
	
	return None

