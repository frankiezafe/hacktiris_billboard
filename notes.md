## workflow

### posting

updating billboard happens via email to billboard@hactiris.be 

1. user sends an email with any subject and body
2. server replies with a std message explaining how to get templates for event, info or notifications
3. user send a template request
4. if request is correct, template is sent, if not, std message is sent back
5. user complete the template
6. server receive and validate template, create a staging version of the post and send a validation email
7. user has to confirm - if not, the staging version after 7 days
8. if confirm, the server push the info to the billboard

### management

server folders structure:

- **history**: stores unsuccessfull requests and old pages
- **cache**: contains all pages up-to-date ready to be displayed in the billboard : txt format
- **staging**: each request is a subfolder containing the mail, the txt version and the media
- **hotfolder**: once confirmed, the request are moved in this folder - a cronjob is fired to update the cache

### security

- **whitelist**: json [optional], contains a list of all emails allowed to interact freely with the system
- **blacklist**: json [optional], contains a list of all emails discarded by default
- **roles**: json [mandatory], contains a list of emails with authorisations, mainly admins

### services

- **mailer**: responsible of reading and sending emails to user, works with staging & hofolder
- **layouter**: organise events, generates pages, hofolder > cache
- **archiver**: cleanup cache, staging & hotfolder > history
- **billboard**: basically a slideshow manager, reads the cache folder and renders pages in FE

mailer runs every 30 seconds and check emails

layouter is called by mailer once a post is validated

archiver runs once a day

billboard is running constantly @ 15 fps : enables animations