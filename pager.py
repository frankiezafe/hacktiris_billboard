# https://www.geeksforgeeks.org/formatted-text-linux-terminal-using-python/

import os
import pyfiglet
import termcolor
import time

import imager
import gifer
import tabler

os.system('clear')

rows, cols = os.popen('stty size', 'r').read().split()
rows = int( rows )
cols = int( cols )

f = pyfiglet.Figlet(font='slscript', width=cols)
title = f.renderText('Hacktiris Billboard')

table_cfg = tabler.tabler_config()
table_cfg[ 'table_columns' ] = 1
table_cfg[ 'dump' ] = True
table_cfg[ 'cell_border' ] = 0
table_cfg[ 'viewport' ][0] = 100
table_cfg[ 'vertical_align' ] = 'center'
table_cfg[ 'cell_padding' ] = [0,0,0,0]
table_cfg[ 'cell_style_even' ]  = '\033[46m\033[1;33m'
tabler.quick_table( title, table_cfg )

######### IMAGE DEMO

'''
im = imager.covertImageToAscii('assets/LogoActirisFr.png', 200, 0.43, True)
for l in im:
	# center the image
	offset = int( ( cols - rows ) * 0.5 )
	prepend = ""
	while offset > 0:
		prepend += " "
		offset -= 1
	print( prepend + l )

f = open('out.txt', 'w')
for row in im:
	f.write(row + '\n') 
f.close()
'''

######### GIF DEMO

'''
#gifer.tmp()
animation = gifer.load_anim( 'assets/goat.gif.txt' )
while 1:
	os.system('clear')
	gifer.print_next_frame( animation, 3 )
	time.sleep(0.1)
'''