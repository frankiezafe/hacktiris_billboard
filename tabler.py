import os
import sys
import time
import copy

def process_cell( config, cell, cid ):
	
	txt = cell['content'].replace( '\p', '\n\n' ).replace( '\t', '    ' )
	cell['lines'] = []
	
	paragraphs = txt.split( '\n' )
	words = []
	for p in paragraphs:
		words.append( p.split( ' ' ) )
	
	cell['cfg']['cell_inner'] = config['cell_outer'][ cid ] - ( cell['cfg']['cell_padding'][1] + cell['cfg']['cell_padding'][3] )
	
	for ws in words:
		s = ''
		for w in ws:
			wl = len( w ) + 1 # will print a space after each word
			if cell['cfg']['word_wrap'] == 'word':
				if wl < cell['cfg']['cell_inner'] and len(s) + wl >= cell['cfg']['cell_inner']:
					if cell['cfg']['cell_maxh'] != -1 and len( cell['lines'] ) >= cell['cfg']['cell_maxh']:
						break
					cell['lines'].append( s )
					s = ''
				while wl > cell['cfg']['cell_inner']:
					cutat = cell['cfg']['cell_inner'] - len(s)
					s += w[ : cutat ]
					w = w[ cutat : ]
					if cell['cfg']['cell_maxh'] != -1 and len( cell['lines'] ) >= cell['cfg']['cell_maxh']:
						break
					cell['lines'].append( s )
					s = ''
					wl = len( w )
				s += w + ' '
			elif cell['cfg']['word_wrap'] == 'none':
				for letter in range( wl ):
					if len(s) >= cell['cfg']['cell_inner']:
						break
					if letter > wl - 2:
						s += config['empty_char']
					else:
						s += w[ letter ]
			else:
				for letter in range( wl ):
					if len(s) >= cell['cfg']['cell_inner']:
						cell['lines'].append( s )
						s = ''
					if letter > wl - 2:
						s += config['empty_char']
					else:
						s += w[ letter ]
		if cell['cfg']['cell_maxh'] != -1 and len( cell['lines'] ) >= cell['cfg']['cell_maxh']:
			break
		cell['lines'].append( s )
	
	if cell['cfg']['cell_maxh'] != -1 and len( cell['lines'] ) == cell['cfg']['cell_maxh']:
		llid = len(cell['lines']) - 1
		ltm = len( config['truncated_marker'] )
		
		if len( cell['lines'][ llid ] ) + ltm >= cell['cfg']['cell_inner']:
			cell['lines'][llid] = cell['lines'][llid][0:cell['cfg']['cell_inner'] - ltm ]
		cell['lines'][llid] += config['truncated_marker']

def new_table_line( config ):
	ll = config['table_width'] + ( config['table_columns'] + 1 ) * config['cell_border']
	nl = [ config[ 'empty_char' ] for i in range( ll ) ]
	return nl

def get_table_line( config, lines, i ):
	while i >= len( lines ):
		lines.append( new_table_line( config ) )
	return lines[i]

def cell_packing_info():
	return {
		'totalh':0,
		'top':0,
		'left_padding':0,
		'lstart':0,
		'cell': None,
		'cellid': 0,
		'offset': 0,
		'bnum': 0,
		# V packing specific
		'columni': 0,
		'offseth': 0 
	}

def fluid_H_packing( config, cells ):
	
	# packing cells
	rows_height = [ 0 for i in range( config['table_columns'] ) ]
	lines = []
	cellid = 0
	
	cpi = cell_packing_info()

	for cell in cells:
		
		# selecting the right columns
		cid = 0
		for i in range( config['table_columns'] ):
			if rows_height[i] < rows_height[cid]:
				cid = i
		
		cpi['bnum'] = 1
		# detecting first row and adding an extra line of border
		if rows_height[cid] == 0:
			cpi['bnum'] = 2
		
		process_cell( config, cells[cellid], cid )
		
		cpi['totalh'] = len( cell['lines'] ) + config['cell_border'] * cpi['bnum'] + cell['cfg']['cell_padding'][0] + cell['cfg']['cell_padding'][2]
		cpi['top'] = config['cell_border'] * ( cpi['bnum'] - 1 )
		cpi['cell'] = cells[cellid]
		cpi['left_padding'] = config['cell_border'] + cell['cfg']['cell_padding'][3]
		cpi['cellid'] = cellid
		cpi['columni'] = cid
		cpi['offset'] = config['cell_offset'][ cid ] + ( cid * config['cell_border'] )
		cpi['offseth'] = rows_height[cid]
		cell_pack( config, lines, cpi )

		cellid += 1
		rows_height[cid] += cpi['totalh']
		
	return lines

def fluid_V_packing( config, cells ):
	
	lines = []
	current_col = 0
	current_height = 0
	
	cpi = cell_packing_info()
	
	# copy of the config, to restore at the end of the process
	config_bu = {}
	for k in config:
		config_bu[k] = copy.deepcopy(config[k])
	
	# first run to process number of columns
	for cellid in range( len( cells ) ):
		
		if current_height >= config['canvas'][1]:
			current_col += 1
			current_height = 0
		
		# table overflow! let's generate new columns of standard width
		if current_col >= config['table_columns']:
			cw = int( config['table_width'] / config['table_columns'] )
			cn = len( config['cell_outer'] )
			config['cell_outer'].append(cw)
			config['cell_offset'].append( config['cell_offset'][cn-1] + config['cell_outer'][cn-1] )
		
		process_cell( config, cells[cellid], current_col )
		
		cpi['bnum'] = 1
		if current_height == 0:
			cpi['bnum'] = 2
		
		cell = cells[cellid]
		cpi['totalh'] = len( cell['lines'] ) + config['cell_border'] * cpi['bnum'] + cell['cfg']['cell_padding'][0] + cell['cfg']['cell_padding'][2]
		
		if ( cpi['bnum'] == 1 and cpi['totalh'] >= config['canvas'][1] ) or current_height + cpi['totalh'] > config['canvas'][1]:
			current_col += 1
			current_height = 0
			cpi['bnum'] = 2
			cpi['totalh'] = len( cell['lines'] ) + config['cell_border'] * cpi['bnum'] + cell['cfg']['cell_padding'][0] + cell['cfg']['cell_padding'][2]
		
		current_height += cpi['totalh']
	
	if current_col >= config['table_columns']:
		config['table_columns'] = current_col + 1
	
	current_col = 0
	current_height = 0
	
	config['table_width'] = 0
	for i in config['cell_outer']:
		config['table_width'] += i
	
	# now generation of content
	for cellid in range( len( cells ) ):
		
		cell = cells[cellid]
		
		if current_height >= config['canvas'][1]:
			current_col += 1
			current_height = 0
		
		cpi['bnum'] = 1
		if current_height == 0:
			cpi['bnum'] = 2
		
		cpi['totalh'] = len( cell['lines'] ) + config['cell_border'] * cpi['bnum'] + cell['cfg']['cell_padding'][0] + cell['cfg']['cell_padding'][2]
		
		# preventing vertical overflow if cell height is smaller than canvas height
		if ( cpi['bnum'] == 1 and cpi['totalh'] >= config['canvas'][1] ) or current_height + cpi['totalh'] > config['canvas'][1]:
			current_col += 1
			current_height = 0
			cpi['bnum'] = 2
			cpi['totalh'] = len( cell['lines'] ) + config['cell_border'] * cpi['bnum'] + cell['cfg']['cell_padding'][0] + cell['cfg']['cell_padding'][2]
				
		cpi['top'] = config['cell_border'] * ( cpi['bnum'] - 1 )
		cpi['cell'] = cell
		cpi['cellid'] = cellid
		cpi['left_padding'] = config['cell_border'] + cell['cfg']['cell_padding'][3]
		cpi['columni'] = current_col
		cpi['offset'] = config['cell_offset'][current_col] + ( current_col * config['cell_border'] )
		cpi['offseth'] = current_height
		cell_pack( config, lines, cpi )
		
		current_height += cpi['totalh']
	
	# restoring config
	for k in config:
		config[k] = config_bu[k]
	
	return lines

def strict_V_packing( config, cells ):
	
	# first task here is to find the optimal number of cells in 1 column,
	# knowing that all cells of the same row will have the same height
	
	rnum = int( len( cells ) / config['table_columns'] )
	if rnum * config['table_columns'] < len( cells ):
		rnum += 1
	table_rows = [0 for i in range( rnum )]
	
	for ci in range( len(cells) ):
		li = ci % rnum
		process_cell( config, cells[ci], int( ci / rnum ) )
		cell = cells[ci]
		cl = len( cell['lines'] ) + cell['cfg']['cell_padding'][0] + cell['cfg']['cell_padding'][2]
		if cl > table_rows[li]:
			table_rows[li] = cl
	
	lines = []
	
	cpi = cell_packing_info()
	cpi['offseth'] = 0
	
	# packing lines
	for ri in range( rnum ):
		
		tr = table_rows[ ri ]
		
		cpi['bnum'] = 1
		# detecting first row and adding an extra line of border
		if ri == 0:
			cpi['bnum'] = 2
		
		cpi['totalh'] = tr + config['cell_border'] * cpi['bnum']
		cpi['top'] = config['cell_border'] * ( cpi['bnum'] - 1 )
		cpi['lstart'] = len(lines)
		
		for cellid in range( ri, len(cells), rnum ):
			cell = cells[cellid]
			cpi['cell'] = cell
			cpi['left_padding'] = config['cell_border'] + cell['cfg']['cell_padding'][3]
			cpi['cellid'] = cellid
			cpi['columni'] = int( cellid / rnum )
			cpi['offset'] = config['cell_offset'][ cpi['columni'] ] +  + ( cpi['columni'] * config['cell_border'] )
			cell_pack( config, lines, cpi )

		cpi['offseth'] += cpi['totalh']

	return lines

def strict_H_packing( config, cells ):
	
	# first task here is to find the optimal number of cells in 1 column,
	# knowing that all cells of the same row will have the same height
	
	lines = []
	cpi = cell_packing_info()
	cpi['offseth'] = 0
	cpi['cellid'] = 0
	cpi['bnum'] = 2
	
	while cpi['cellid'] < len( cells ):
		
		tallest = 0
		for i in range( config['table_columns'] ):
			if cpi['cellid'] + i >= len( cells ):
				break
			process_cell( config, cells[ cpi['cellid'] + i ], i )
			cell = cells[ cpi['cellid'] + i ]
			ll = len( cell['lines'] ) + cell['cfg']['cell_padding'][0] + cell['cfg']['cell_padding'][2]
			if tallest < ll:
				tallest = ll
		
		cpi['totalh'] = tallest + config['cell_border'] * cpi['bnum']
		cpi['top'] = config['cell_border'] * ( cpi['bnum'] - 1 )
		cpi['lstart'] = len(lines)
		
		for i in range( config['table_columns'] ):
			
			if cpi['cellid'] >= len( cells ):
				return lines
			
			cell = cells[ cpi['cellid'] ]
			cpi['cell'] = cell
			cpi['left_padding'] = config['cell_border'] + cell['cfg']['cell_padding'][3]
			cpi['columni'] = i
			cpi['offset'] = config['cell_offset'][ cpi['columni'] ] + ( cpi['columni'] * config['cell_border'] )
			cell_pack( config, lines, cpi )
			
			cpi['cellid'] += 1
		
		cpi['offseth'] += cpi['totalh']
		cpi['bnum'] = 1
	
	return lines

def cell_pack( config, lines, cpi ):
	
	cell_lid = 0
	cell = cpi['cell']
	if cell == None:
		return
	
	for i in range( cpi['totalh'] ):
				
		l = get_table_line( config, lines, cpi['offseth'] + i )

		if ( cpi['bnum'] == 2 and i < config['cell_border'] ) or i >= cpi['totalh'] - config['cell_border']:
			
			# adding top & bottom line
			for b in range( config['cell_outer'][ cpi['columni'] ] + config['cell_border'] * 2 ):
				l[ cpi['offset'] + b ] = config['border_char']
		
		else:
			
			# adding left & right borders

			for b in range( config['cell_border'] ):
				l[ cpi['offset'] + b ] = config['border_char']
			for b in range( config['cell_outer'][ cpi['columni'] ] + config['cell_border'], config['cell_outer'][ cpi['columni'] ] + config['cell_border'] * 2 ):
				l[ cpi['offset'] + b ] = config['border_char']
			
			# adding left & right padding
			
			for b in range( config['cell_border'], cpi['left_padding'] ):
				l[ cpi['offset'] + b ] = config['padding_char']
				
			for b in range( config['cell_outer'][ cpi['columni'] ] + config['cell_border'] - cell['cfg']['cell_padding'][1], config['cell_outer'][ cpi['columni'] ] + config['cell_border'] ):
				l[ cpi['offset'] + b ] = config['padding_char']
				
			# pushing the lines
			
			if ( i >= cpi['top'] and i < cpi['top'] + cell['cfg']['cell_padding'][0] ) or ( i >= cpi['totalh'] - ( cell['cfg']['cell_padding'][2] + config['cell_border'] ) and i < cpi['totalh'] - config['cell_border'] ):
				for b in range( cell['cfg']['cell_inner'] ):
					l[ cpi['offset'] +cpi['left_padding'] + b ] = config['padding_char']
			else:
				if cell_lid < len( cell['lines'] ):
					ll = len( cell['lines'][ cell_lid ] )
					loc_offset = 0
					
					# vertical alignment
					if ll < cell['cfg']['cell_inner']:
						if cell['cfg']['vertical_align'] == 'center':
							loc_offset = int(  ( cell['cfg']['cell_inner'] - ll ) / 2 )
						elif cell['cfg']['vertical_align'] == 'right':
							loc_offset = cell['cfg']['cell_inner'] - ll
					for b in range( loc_offset ):
						l[ cpi['offset'] + cpi['left_padding'] + b ] = config['empty_char']
					
					# and, eventually, pushing the text
					for b in range( ll ):
						l[ cpi['offset'] + cpi['left_padding'] + loc_offset + b ] = cell['lines'][ cell_lid ][ b ]
					
				cell_lid += 1
				
		# adding styles
				
		if config['cell_style_enabled']:
			
			if config['cell_border'] > 0 and len(config['border_color']) > 0:
				l[ cpi['offset'] ] = config['border_color'] + l[ cpi['offset'] ]
				l[ cpi['offset'] + config['cell_outer'] + config['cell_border'] - 1 ] += config['border_color']
				cp0 = cpi['offset'] + config['cell_outer'] + config['cell_border'] * 2 - 1
				cp1 = cpi['offset'] + config['cell_outer'] + config['cell_border'] * 2
				if cp0 < len( l ) and l[ cp0 ] == config[ 'border_char' ] and ( cp1 >= len( l ) or l[ cp1 ] != config[ 'border_char' ] ):
					 l[ cp0 ] += config['style_reset']
			else:
				l[ cpi['offset'] + config['cell_outer'][cpi['columni']] + config['cell_border'] - 1 ] += config['style_reset']
			
			if i >= cpi['top'] and i < cpi['totalh'] - (config['cell_border']):
				ci = cpi['offset'] + config['cell_border']
				if cpi['cellid'] % 2 == 0:
					l[ci] = config['cell_style_even'] + l[ci]
				else:
					l[ci] = config['cell_style_odd'] + l[ci]

def render_lines( config, lines, rect = None ):
	s = ''
	if config['display_rect'] == None:
		for l in lines:
			s += ''.join(l) + '\n'
	else:
		lmax = len( lines )
		if lmax > config['display_rect'][1] + config['display_rect'][3]:
			lmax = config['display_rect'][1] + config['display_rect'][3]
		for i in range( config['display_rect'][1], lmax ):
			if i >= len( lines ):
				break
			if i < 0:
				s += '\n'
				continue
			xmin = config['display_rect'][0]
			xmax = config['display_rect'][2]
			if config['display_rect'][0] < 0:
				s += ''.join( [ config[ 'empty_char' ] for j in range( -config['display_rect'][0] ) ] )
				xmax += config['display_rect'][0]
				xmin = 0
			endline = '\n'
			if config['cell_style_enabled']:
				endline = config[ 'style_reset' ] + endline
			s += ''.join(lines[i][xmin:xmax]) + endline
	return s

def tabler_config( src = None ):
	
	if src == None:

		rows, cols = os.popen('stty size', 'r').read().split()
		rows = int( rows )
		cols = int( cols )

		return {
			# ********************** general **********************
			'canvas': [ cols, rows ],
			'display_rect': [ 0, 0, cols, rows ],
			'table_columns': 4,
			'columns_width': None,						# a list of columns width (between 0 and 1), will be generated by table if None
			'packing': 'strict_H', 						# can be strict_H (default), strict_V, fluid_H or fluid_V
			'export_path': 'tmp/tabler.py-output.txt',
			'dump': False,
			'return': 'cropped',						# can be cropped (default), lines, cells, size or none
			'word_wrap': 'letter', 						# can be letter (default), word or none
			# ********************** styling **********************
			'cell_maxh': -1,
			'cell_border': 1,
			'cell_padding': [1,2,1,2],
			'vertical_align': 'left', 					# can be left (default), center or right
			'border_char': '·',
			'padding_char': ' ',
			'empty_char': ' ',
			'truncated_marker': '[...]',
			'cell_style_enabled': True,
			'cell_style_even': '\033[40m\033[1;37m',
			'cell_style_odd': '\033[47m\033[1;31m',
			'border_color': '',
			'style_reset': '\033[m',
			# ********************** processed **********************
			'table_width': 0, 							# based on canvas size
			'cell_outer': None, 						# list of width in integer
			'cell_offset': None							# list of horizontal offset in integer
		}
	
	else:
		out = {}
		for k in src:
			out[k] = copy.deepcopy( src[k] )
		return out

def cell_config( config = None ):
	if config == None:
		config = tabler_config()
	return {
		'vertical_align': config['vertical_align'],
		'word_wrap': config['word_wrap'],
		'cell_padding': config['cell_padding'],
		'cell_maxh': None,							# if left to none, it will be overwritten by the table config one
		'cell_inner': 0 							# will be processed by process_cell
	}

def cell_data( txt, config = None ):
	return {
		'content' : txt,
		'cfg': cell_config( config ),
		'lines': []
	}

def tabler( data, config ):
	
	config['table_width'] = config['canvas'][0] - ( config['cell_border'] * ( config['table_columns'] + 1 ) )
	
	cwidth = [ 1 / config['table_columns'] for i in range( config['table_columns'] ) ]
	
	if config['columns_width'] != None:
		while len( config['columns_width'] ) != config['table_columns']:
			config['columns_width'].append( 1 )
		# normalisation
		total = 0
		for i in range( config['table_columns'] ):
			total += config['columns_width'][i]
		for i in range( config['table_columns'] ):
			config['columns_width'][i] /= total
	else:
		config['columns_width'] = cwidth
	
	# turning percentages into character num:
	
	config['cell_outer'] = [ 0 for i in range( config['table_columns'] ) ]
	config['cell_offset'] = [ 0 for i in range( config['table_columns'] ) ]
	
	total = 0
	errors = [ 0 for i in range( config['table_columns'] ) ]
	for i in range( config['table_columns'] ):
		w = config['table_width'] * config['columns_width'][i]
		config['cell_outer'][i] = int( w )
		if config['cell_outer'][i] == 0:
			config['cell_outer'][i] = 1
		else:
			errors[i]  = w -int( w )
		total += config['cell_outer'][i]
	
	# error diffusion
	err = 0
	for i in range( config['table_columns'] ):
		err += errors[i]
		if err >= 1:
			config['cell_outer'][i] += 1
			err -= 1
	
	total = 0
	for i in range( config['table_columns'] ):
		config['cell_offset'][ i ] = total
		total += config['cell_outer'][i]
	
	if config['return'] == 'size':
		return None
	
	default_cell_cfg = cell_config( config )
	cells = []
	for d in data:
		cell = None
		if isinstance( d, str ):
			cell = cell_data( d, config )
		else: 
			if 'content' not in d:
				print( 'Invalid cell! use taber.cell_data( txt ) to generate a correct structure' )
				continue
			if 'lines' not in d or not isinstance( d['lines'], list ):
				d['lines'] = []
			if 'cfg' not in d or not isinstance( d['cfg'], dict ):
				d['cfg'] = {}
			for k in default_cell_cfg:
				if not k in d['cfg']:
					d['cfg'][k] = default_cell_cfg[k]
			cell = d.copy()
		if cell['cfg']['cell_maxh'] == None:
			cell['cfg']['cell_maxh'] = config['cell_maxh']
		#process_cell( config, cell )
		cells.append( cell )
	
	lines = []
	
	if config['packing'] == 'fluid_H':
		lines = fluid_H_packing( config, cells )
	elif config['packing'] == 'fluid_V':
		lines = fluid_V_packing( config, cells )
	elif config['packing'] == 'strict_H':
		lines = strict_H_packing( config, cells )
	else:
		lines = strict_V_packing( config, cells )
	
	rls = render_lines( config, lines )
	
	if config['dump']:
		sys.stdout.write( rls )
	
	if config['export_path'] != '':
		f = open( config['export_path'], 'w' )
		f.write( rls )
		f.close()

	if config['return'] == 'cells':
		return cells
	elif config['return'] == 'lines':
		return lines
	elif config['return'] == 'cropped':
		return rls
	else:
		return None

def quick_table( txt, config ):
	return tabler( [txt], config )
	
	
# UNCOMMENT THIS BLOCK TO SEE A DEMO OF THE TABLER
'''

info = [
	{ 
		'content':"          _       _  \n        / /\    / /\ \n       / / /   / / / \n      / /_/   / / /  \n     / /\ \__/ / /   \n    / /\ \___\/ /    \n   / / /\/___/ /     \n  / / /   / / /      \n / / /   / / /       \n/ / /   / / /        \n\/_/    \/_/         \n                     ",
		'lines': [],
		'cfg': {
			'vertical_align': 'center',
			'word_wrap': 'none',
			'cell_padding': [ 2,0,2,0 ],
			'cell_maxh': -1
		}
	},
	"[1] Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum et consectetur massa, placerat pretium turpis. Sed sed nisl efficitur, aliquet libero vitae, ullamcorper massa. Sed condimentum, magna bibendum ultrices malesuada, neque nibh porttitor est, ut vestibulum augue neque sed justo. Quisque id feugiat ante. Donec tincidunt turpis nunc, eu lobortis nibh lobortis volutpat. Nam luctus sem eu nisi finibus blandit. Morbi viverra a elit vitae ultrices.\pEtiam et velit dapibus dolor bibendum faucibus. Sed ut ex bibendum, pulvinar elit a, viverra libero. Phasellus facilisis placerat pulvinar. Quisque egestas ullamcorper ex. Praesent sed lacus id eros porta varius. Duis efficitur turpis eget eleifend vestibulum. Suspendisse felis augue, euismod vitae lacinia ac, congue id massa.",
	"[2] python suite to create billboards with multi pages, timer, animation and image transformation into ascii python suite to create billboards with multi pages, timer, animation and image transformation into ascii python suite to create billboards with multi pages, timer, animation and image transformation into ascii",
	"[3] https://twoglasshams.itch.io/ashi-wash-classic",
	"[4] render form each side in png, orthogonal projection relocate the origin",
	"[5]\n\t- https://robotvault.bitbucket.io/scenenet-rgbd.htmlhttps://robotvault.bitbucket.io/scenenet-rgbd.html\n\t- https://carlburton.itch.io/islands",
	"[6] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed auctor mauris, eget tincidunt sem. In eu malesuada enim. Donec placerat nisl vel velit rhoncus rutrum. Mauris pulvinar mi ut dui lobortis rhoncus imperdiet et urna. Proin tellus mauris, porttitor commodo porttitor sit amet, tristique in leo. Curabitur quis molestie nibh. Vestibulum id volutpat augue, sit amet porta nulla. Donec gravida justo tellus, eleifend cursus neque pulvinar laoreet. ",
	"[7] Aenean sed tincidunt lectus, id viverra justo. Curabitur a fermentum ante. Suspendisse tortor lacus, varius nec viverra et, pulvinar at metus. Duis fringilla metus sit amet arcu suscipit, ut tristique metus vulputate. Aliquam eu ex fermentum, maximus diam id, sollicitudin orci. Integer non hendrerit magna, non vehicula lectus. Suspendisse dapibus, leo luctus mollis dignissim, quam risus consequat ligula, euismod lacinia mauris dui non ipsum. Nulla hendrerit metus ipsum, in aliquam urna mollis at. Sed egestas hendrerit posuere. Donec eleifend a diam et auctor. Morbi nibh tellus, pretium nec condimentum et, accumsan tristique ante. Morbi suscipit enim nibh, eget efficitur risus lobortis et. ",
	"[8] Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum et consectetur massa, placerat pretium turpis. Sed sed nisl efficitur, aliquet libero vitae, ullamcorper massa. Sed condimentum, magna bibendum ultrices malesuada, neque nibh porttitor est, ut vestibulum augue neque sed justo. Quisque id feugiat ante. Donec tincidunt turpis nunc, eu lobortis nibh lobortis volutpat. Nam luctus sem eu nisi finibus blandit.\n\nMorbi viverra a elit vitae ultrices. Etiam et velit dapibus dolor bibendum faucibus. Sed ut ex bibendum, pulvinar elit a, viverra libero. Phasellus facilisis placerat pulvinar. Quisque egestas ullamcorper ex. Praesent sed lacus id eros porta varius. Duis efficitur turpis eget eleifend vestibulum. Suspendisse felis augue, euismod vitae lacinia ac, congue id massa.",
	"[10] https://twoglasshams.itch.io/ashi-wash-classic",
	"[11] render form each side in png, orthogonal projection relocate the origin",
	"",
	"[12] Aenean sed tincidunt lectus, id viverra justo. Curabitur a fermentum ante. Suspendisse tortor lacus, varius nec viverra et, pulvinar at metus. Duis fringilla metus sit amet arcu suscipit, ut tristique metus vulputate. Aliquam eu ex fermentum, maximus diam id, sollicitudin orci. Integer non hendrerit magna, non vehicula lectus. Suspendisse dapibus, leo luctus mollis dignissim, quam risus consequat ligula, euismod lacinia mauris dui non ipsum. Nulla hendrerit metus ipsum, in aliquam urna mollis at. Sed egestas hendrerit posuere. Donec eleifend a diam et auctor. Morbi nibh tellus, pretium nec condimentum et, accumsan tristique ante. Morbi suscipit enim nibh, eget efficitur risus lobortis et.",
	"[13]\n- https://robotvault.bitbucket.io/scenenet-rgbd.html\n- https://carlburton.itch.io/islands",
	"[14] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed auctor mauris, eget tincidunt sem. In eu malesuada enim. Donec placerat nisl vel velit rhoncus rutrum. Mauris pulvinar mi ut dui lobortis rhoncus imperdiet et urna. Proin tellus mauris, porttitor commodo porttitor sit amet, tristique in leo. Curabitur quis molestie nibh. Vestibulum id volutpat augue, sit amet porta nulla. Donec gravida justo tellus, eleifend cursus neque pulvinar laoreet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed auctor mauris, eget tincidunt sem. In eu malesuada enim. Donec placerat nisl vel velit rhoncus rutrum. Mauris pulvinar mi ut dui lobortis rhoncus imperdiet et urna. Proin tellus mauris, porttitor commodo porttitor sit amet, tristique in leo. Curabitur quis molestie nibh. Vestibulum id volutpat augue, sit amet porta nulla. Donec gravida justo tellus, eleifend cursus neque pulvinar laoreet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed auctor mauris, eget tincidunt sem. In eu malesuada enim. Donec placerat nisl vel velit rhoncus rutrum. Mauris pulvinar mi ut dui lobortis rhoncus imperdiet et urna. Proin tellus mauris, porttitor commodo porttitor sit amet, tristique in leo. Curabitur quis molestie nibh. Vestibulum id volutpat augue, sit amet porta nulla. Donec gravida justo tellus, eleifend cursus neque pulvinar laoreet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed auctor mauris, eget tincidunt sem. In eu malesuada enim. Donec placerat nisl vel velit rhoncus rutrum. Mauris pulvinar mi ut dui lobortis rhoncus imperdiet et urna. Proin tellus mauris, porttitor commodo porttitor sit amet, tristique in leo. Curabitur quis molestie nibh. Vestibulum id volutpat augue, sit amet porta nulla. Donec gravida justo tellus, eleifend cursus neque pulvinar laoreet. "
]

os.system('clear')

cfg = tabler_config()
cfg['return'] = 'cropped'
cfg['packing'] = 'fluid_H'
cfg['canvas'][0] -= 20
cfg['canvas'][1] -= 10
cfg['display_rect'][3] = cfg['canvas'][1]
cfg['cell_maxh'] = -1

print( "************* FLUID H *************" )
out = tabler( info, cfg )
print( out )

print( "************* FLUID V *************" )

cfg['packing'] = 'fluid_V'
out = tabler( info, cfg )
print( out )

print( "************* STRICT H *************" )

cfg['packing'] = 'strict_H'
out = tabler( info, cfg )
print( out )

print( "************* STRICT V *************" )

cfg['packing'] = 'strict_V'
cfg['columns_width'] = [0.3,0.2,0.2,0.3]
out = tabler( info, cfg )
print( out )

'''