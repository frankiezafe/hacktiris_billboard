from PIL import Image
import subprocess
from os import listdir
from os.path import isfile, join
import imager

import_gif = 'assets/goat.gif'
export_path = 'assets/goat/'

def split( gif_path ):
	
	if gif_path[:-3] != 'gif':
		return

def load_anim( anim_path ):
	
	data = open( anim_path, 'r' )
	frames = None
	index = -1
	tmp_frame = []
	for l in data:
		
		if l[:8] == '#frames:':
			frames = [[] for i in range( int( l[8:] ) ) ]
			print( frames )
		elif frames != None:
			if l[:7] == '#frame:':
				if index != -1:
					frames[index] = tmp_frame
				index = int( l[7:] )
				tmp_frame = []
			else:
				tmp_frame.append( l[:-1] )
	
	animation = {}
	animation['frames'] = frames
	animation['current_frame'] = 0
	
	return animation

def get_next_frame( animation, step = 1 ):
	
	f = animation['frames'][animation['current_frame']]
	animation['current_frame'] += step
	animation['current_frame'] %= len( animation['frames'] )
	return f

def print_next_frame( animation, step = 1 ):
	
	f = get_next_frame( animation, step )
	for l in f:
		print( l )

def tmp():
	
	# let's use ffmpeg to split the gif
	cmd = 'ffmpeg -i ' + import_gif + ' -vsync 0 ' + export_path + '%05d.png'
	#subprocess.call( cmd.split( ' ' ) )

	# and imager to turn the animation into a big text file
	pngs = [join(export_path, f) for f in listdir(export_path) if isfile(join(export_path, f))]
	pngs.sort()
	print( pngs )

	animf = open( import_gif + '.txt', 'w' )
	animf.write( '#frames:' + str( len( pngs ) ) + '\n' )
	fi = 0
	for png in pngs:
		im = imager.covertImageToAscii(png, 120, 0.43, True)
		animf.write( '#frame:' + str( fi ) + '\n' )
		for l in im:
			animf.write( l + '\n' )
			pass
		fi += 1
	animf.close()