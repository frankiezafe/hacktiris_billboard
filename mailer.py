import os
import smtplib
import time
import imaplib
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import base64
import json
import re
import copy
import dateparser
import shutil

import billboard_globals as bbglob
import layouter
from access import hbb

bbglob.check_folder( bbglob.FOLDER_STAGING )
bbglob.check_folder( bbglob.FOLDER_HOTFOLDER )

whitelist = []
blacklist = []
roles = []

def load_controls():
	
	global whitelist
	global blacklist
	global roles
	
	bbglob.check_file( bbglob.CONTROL_BLACK_LIST )
	if os.path.isfile( bbglob.CONTROL_BLACK_LIST ):
		with open( bbglob.CONTROL_BLACK_LIST ) as f:
			blacklist = json.load(f)
	else:
		
		update = True
	
	bbglob.check_file( bbglob.CONTROL_WHITE_LIST )
	if os.path.isfile( bbglob.CONTROL_WHITE_LIST ):
		with open( bbglob.CONTROL_WHITE_LIST ) as f:
			whitelist = json.load(f)
	
	bbglob.check_file( bbglob.CONTROL_ROLES )
	if os.path.isfile( bbglob.CONTROL_ROLES ):
		with open( bbglob.CONTROL_ROLES ) as f:
			roles = json.load(f)
	else:
		autorise_user( hbb.IMAP_user, bbglob.Role.ADMIN )
	
	# checking lists
	update = False
	for r in roles:
		if not r['email'] in whitelist:
			whitelist.append( r['email'] )
			update = True
		if r['email'] in blacklist:
			blacklist.remove( r['email'] )
			update = True
	for e in blacklist:
		if e in whitelist:
			whitelist.remove( e )
			update = True
	
	if update:
		with open( bbglob.CONTROL_WHITE_LIST, 'w') as f:
			json.dump( whitelist, f )
		with open( bbglob.CONTROL_BLACK_LIST, 'w') as f:
			json.dump( blacklist, f )

def blacklist_user( emailaddr ):
	
	if emailaddr in whitelist:
		whitelist.remove( emailaddr )
		with open( bbglob.CONTROL_WHITE_LIST, 'w') as f:
			json.dump( whitelist, f )
	
	for r in roles:
		if r['email'] == emailaddr:
			roles.remove( r )
			with open( bbglob.CONTROL_ROLES, 'w') as f:
				json.dump( roles, f )
			break
	
	if emailaddr in blacklist:
		return False
	
	blacklist.append( emailaddr )
	with open( bbglob.CONTROL_BLACK_LIST, 'w') as f:
		json.dump( blacklist, f )
	
	bbglob.log( bbglob.LOG_MAIL, 'USER:: ' + emailaddr + ' is blacklisted' )
	
	return True

def autorise_user( emailaddr, role = bbglob.Role.EDITOR ):
	
	if emailaddr in blacklist:
		bbglob.log( bbglob.LOG_MAIL, 'USER:: ' + emailaddr + ' is blacklisted!' )
		return bbglob.Role.REJECT
	
	updated = False
	for r in roles:
		if r['email'] == emailaddr:
			if r['role'] == int( role ):
				return role
			r['role'] = int( role )
			if not emailaddr in whitelist:
				whitelist.append( emailaddr )
			updated = True
	
	if not updated:
		entry = { 'email': emailaddr, 'role': int( role ) }
		roles.append( entry )
		whitelist.append( emailaddr )
	
	with open( bbglob.CONTROL_WHITE_LIST, 'w') as f:
		json.dump( whitelist, f )
	with open( bbglob.CONTROL_ROLES, 'w') as f:
		json.dump( roles, f )
	
	bbglob.log( bbglob.LOG_MAIL, 'USER:: ' + emailaddr + ' created with role ' + str( entry['role'] ) )
	
	return role

def get_access( msg ):
	
	if not bbglob.BB_EMAIL_FROM in msg:
		return bbglob.Role.ERROR
	
	elif msg[bbglob.BB_EMAIL_FROM] in blacklist:
		return bbglob.Role.REJECT
	
	elif msg[bbglob.BB_EMAIL_FROM] in whitelist:
		for r in roles:
			if r['email'] == msg[bbglob.BB_EMAIL_FROM]:
				if r['role'] == int( bbglob.Role.ADMIN ):
					return bbglob.Role.ADMIN
				elif r['role'] == int( bbglob.Role.EDITOR ):
					return bbglob.Role.EDITOR
		return bbglob.Role.UNKNOWN
	
	return bbglob.Role.UNKNOWN

def get_email_address( txt ):
	
	addrs = txt.split( ' ' )
	for addr in addrs:
		if re.match( r"[^@]+@[^@]+\.[^@]+" , addr ):
			return addr.strip().replace('<','').replace('>','').replace(',','').replace(';','')
	return None

def path_mail_staging( UID ):
	return os.path.join( bbglob.FOLDER_STAGING, UID )

def path_mail_hotfolder( UID ):
	return os.path.join( bbglob.FOLDER_HOTFOLDER, UID )

def path_mail_fields( UID ):
	return os.path.join( bbglob.FOLDER_STAGING, UID, 'fields.json' )

def path_mail_body( UID ):
	return os.path.join( bbglob.FOLDER_STAGING, UID, 'body.txt' )

def path_mail_preview( UID ):
	return os.path.join( bbglob.FOLDER_STAGING, UID, 'preview' )

def search_email( UID ):
	
	mpath = path_mail_staging( UID )
	if bbglob.check_folder( mpath, False ):
		return True
	
	return False

def store_email( msg ):
	
	mpath = path_mail_staging( msg[ bbglob.BB_EMAIL_UNIQUE ] )
	
	if bbglob.check_folder( mpath ):
		#bbglob.log( bbglob.LOG_MAIL, 'ERROR:: mail [' + msg[ bbglob.BB_EMAIL_UNIQUE ] + ']:' + ' is already stored!' )
		return False
		
	try:
		
		mpath = os.path.join( mpath, 'mail.elm' )
		with open( mpath, 'w' ) as out:
			gen = email.generator.Generator(out)
			gen.flatten( msg['content'] )
			
		bbglob.log( bbglob.LOG_MAIL, 'OK:: mail [' + msg[ bbglob.BB_EMAIL_UNIQUE ] + ']: stored' )
			
	except Exception as e:
		
		bbglob.log( bbglob.LOG_MAIL, 'ERROR:: failed to store mail [' + msg[ bbglob.BB_EMAIL_UNIQUE ] + ']: ' + str(e) )
		
	return True
	
def load_template( path, fields = None, message = '' ):
	
	txt_fields = ''
	if fields != None and isinstance( fields, list ):
		for f in fields:
			txt_fields += f[ 'name' ] 
			if 'content' in f:
				txt_fields += f['content']
			txt_fields += '\n\n'
	
	txt = ''
	with open( path, 'r') as fp:
		txt = fp.read()
		txt = txt.replace( '#SUBJECT_FLAG_EVENT#', bbglob.SUBJECT_FLAG_EVENT )
		txt = txt.replace( '#SUBJECT_FLAG_INFO#', bbglob.SUBJECT_FLAG_INFO )
		txt = txt.replace( '#SUBJECT_FLAG_NOTIF#', bbglob.SUBJECT_FLAG_NOTIF )
		txt = txt.replace( '#SUBJECT_FLAG_TEMPLATE#', bbglob.SUBJECT_FLAG_TEMPLATE )
		txt = txt.replace( '#SUBJECT_FLAG_SUBMISSION#', bbglob.SUBJECT_FLAG_SUBMISSION )
		txt = txt.replace( '#SUBJECT_FLAG_END#', bbglob.SUBJECT_FLAG_END )
		txt = txt.replace( '#TEMPLATE_TITLE#', bbglob.TEMPLATE_TITLE )
		txt = txt.replace( '#TEMPLATE_FOOTER#', bbglob.TEMPLATE_FOOTER )
		txt = txt.replace( '#FIELDS#', txt_fields )
		txt = txt.replace( '#MESSAGE#', message )
	return txt

def send_msg( msg, resp, flag ):
	
	try:
		
		smtp = smtplib.SMTP()
		smtp.set_debuglevel(False)
		smtp.connect( hbb.SMTP_server, hbb.SMTP_port )
		smtp.ehlo()
		smtp.starttls()
		smtp.ehlo()
		smtp.login( hbb.SMTP_user, hbb.SMTP_psswrd )
		try:
			smtp.sendmail( hbb.IMAP_user, msg[ bbglob.BB_EMAIL_REPLY ], resp.as_string() )
		finally:
			smtp.quit()
		
		bbglob.log( bbglob.LOG_MAIL, 'OK:: mail [' + msg[ bbglob.BB_EMAIL_REPLY ] + '] sent ' + flag )
		
	except Exception as e:
		
		bbglob.log( bbglob.LOG_MAIL, 'ERROR:: failed to send mail [' + msg[ bbglob.BB_EMAIL_REPLY ] + '] :' + flag + ' - ' + str(e) )

def send_registration_request( msg ):
	
	resp = MIMEMultipart()
	resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_REGISTRATION ), 'plain' ) )
	resp['Subject'] = bbglob.SUBJECT_FLAG_REGISTRATION
	resp['From'] = hbb.SMTP_user
	resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
	send_msg( msg, resp, bbglob.SUBJECT_FLAG_WELCOME )
	
	resp = MIMEMultipart()
	resp.attach( MIMEText( "validation of {}".format( msg[ bbglob.BB_EMAIL_FROM ] ), 'plain' ) )
	resp['Subject'] = bbglob.SUBJECT_FLAG_USER + bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END + ' ' + msg[ bbglob.BB_EMAIL_FROM ]
	resp['From'] = hbb.SMTP_user
	
	for r in roles:
		if r['role'] == int( bbglob.Role.ADMIN ):
			print( 'sending to admin ' + r['email'] )
			resp['To'] = r['email']
			msg[ bbglob.BB_EMAIL_REPLY ] = r['email']
			send_msg( msg, resp, bbglob.SUBJECT_FLAG_USER )

def send_welcome_msg( msg ):
	
	resp = MIMEMultipart()
	resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_WELCOME ), 'plain' ) )
	resp['Subject'] = bbglob.SUBJECT_FLAG_WELCOME
	resp['From'] = hbb.SMTP_user
	resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
	send_msg( msg, resp, bbglob.SUBJECT_FLAG_WELCOME )

def send_tmpl_message( msg, tmpl ):
	
	resp = MIMEMultipart()
	
	if tmpl == bbglob.SUBJECT_FLAG_EVENT:
		resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_EVENT, bbglob.FIELDS_EVENT ), 'plain' ) )
		
	elif tmpl == bbglob.SUBJECT_FLAG_INFO:
		resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_INFO, bbglob.FIELDS_INFO ), 'plain' ) )
		
	elif tmpl == bbglob.SUBJECT_FLAG_NOTIF:
		resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_NOTIF, bbglob.FIELDS_NOTIF ), 'plain' ) )
		
	resp['Subject'] = tmpl + bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END
	resp['From'] = hbb.SMTP_user
	resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
	send_msg( msg, resp, tmpl + bbglob.SUBJECT_FLAG_TEMPLATE )

def admin_message( msg ):
	
	subject = msg['content']['Subject']
	
	if subject != None:
		
		if subject.find( bbglob.SUBJECT_FLAG_USER ) != -1:
			
			user_address = get_email_address( subject )

			if user_address == None:

				resp = MIMEMultipart()
				resp.attach( MIMEText( "can not find email in registration request!!!", 'plain' ) )
				resp['Subject'] = bbglob.SUBJECT_FLAG_USER + bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END + ' FAILED'
				resp['From'] = hbb.SMTP_user
				resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
				send_msg( msg, resp, bbglob.SUBJECT_FLAG_USER )

			else:

				ws = subject.split( ' ' )
				bl_found = False
				admin_found = False
				for w in ws:
					w = w.lower()
					for bw in bbglob.SUBJECT_FLAG_BLACKLIST:
						if w == bw:
							bl_found  =True
							break
					for aw in bbglob.SUBJECT_FLAG_ADMIN:
						if w == aw:
							admin_found  =True
							break

				resp = MIMEMultipart()

				if bl_found:
					# let's blacklist this email!
					if blacklist_user( user_address ):
						resp.attach( MIMEText( "User {} has been blacklisted".format( user_address ), 'plain' ) )
					else:
						resp.attach( MIMEText( "User {} is already blacklisted, another admin should have done it.".format( user_address ), 'plain' ) )
					resp['Subject'] = bbglob.SUBJECT_FLAG_USER + bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END + ' SUCCESS'
					resp['From'] = hbb.SMTP_user
					resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
					send_msg( msg, resp, bbglob.SUBJECT_FLAG_USER )

				elif admin_found:

					# let's create an admin
					r = autorise_user( user_address, bbglob.Role.ADMIN )
					resp['Subject'] = bbglob.SUBJECT_FLAG_USER + bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END
					resp['From'] = hbb.SMTP_user
					resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
					if r == bbglob.Role.ADMIN:
						resp['Subject'] += ' SUCCESS'
						resp.attach( MIMEText( "User {} is now an admin.".format( user_address ), 'plain' ) )
					else:
						resp['Subject'] += ' FAILED'
						resp.attach( MIMEText( "User {} can not be turned into an admin.".format( user_address ), 'plain' ) )
					send_msg( msg, resp, bbglob.SUBJECT_FLAG_USER )

					if r == bbglob.Role.EDITOR:
						umsg = { bbglob.BB_EMAIL_REPLY: user_address }
						send_welcome_msg( umsg )

				else:

					# let's create an editor
					r = autorise_user( user_address )
					resp['Subject'] = bbglob.SUBJECT_FLAG_USER + bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END
					resp['From'] = hbb.SMTP_user
					resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
					if r == bbglob.Role.EDITOR:
						resp['Subject'] += ' SUCCESS'
						resp.attach( MIMEText( "User {} is now an editor.".format( user_address ), 'plain' ) )
					else:
						resp['Subject'] += ' FAILED'
						resp.attach( MIMEText( "User {} can not be turned into an editor.".format( user_address ), 'plain' ) )
					send_msg( msg, resp, bbglob.SUBJECT_FLAG_USER )

					if r == bbglob.Role.EDITOR:
						umsg = { bbglob.BB_EMAIL_REPLY: user_address }
						send_welcome_msg( umsg )

			return True
	
	return False

def get_body( msg ):
	
	for part in msg['content'].walk():
		ctype = part.get_content_type()
		cdispo = str(part.get('Content-Disposition'))
		if ctype == "text/plain" and 'attachment' not in cdispo:
			return part.get_payload(decode=True).decode('utf-8')
	
	return None

def cleanup_field_content( f, body ):
	
	txt = body[ f['start']:f['stop'] ]
	lines = txt.split( '\n' )
	if isinstance( lines, list ):
		for li in range( len( lines ) ):
			lines[li] = lines[li].strip() + ' '
			while( lines[li][:1] == '>' ):
				lines[li] = lines[li][1:]
		txt = ''.join( lines ).strip()
	elif isinstance( lines, str ):
		lines = lines.strip()
		while( lines[:1] == '>' ):
			lines = lines[1:]
		txt = lines
	
	f['content'] = txt
	
	if f['limit'] != -1 and len( txt ) > f['limit']:
		f['content'] = txt[:f['limit']]
	
	if f['type'] == 'DATE':
		f['content'] = str( dateparser.parse( f['content'] ) )
	elif f['type'] == 'TXT':
		pass
	
	f['raw'] = txt

def parse_form( msg, fieldlist ):
	
	body = get_body( msg )
	# saving body in txt
	with open( path_mail_body( msg[ bbglob.BB_EMAIL_UNIQUE ] ), 'w') as f:
		f.write( body )
		f.close()
	
	# splitting content depending on the fields order
	fields = copy.deepcopy( fieldlist )
	start = 0
	prevf = -1
	for fi in range( len(fields) ):
		fields[fi]['content'] = None
		fields[fi]['raw'] = None
		fp = body.find( fields[fi]['name'] )
		if fp == -1:
			fields[fi]['found'] = False
		else:
			fields[fi]['found'] = True
			if prevf != -1:
				fields[prevf]['stop'] = fp
			fields[fi]['start'] = fp + len( fields[fi]['name'] )
			prevf = fi
	
	end = body.find( bbglob.TEMPLATE_FOOTER )
	if end != -1:
		fields[ len(fields) -1 ]['stop'] = end
	else:
		fields[ len(fields) -1 ]['stop'] = len( body )
	
	valid = True
	for f in fields:
		if f['found']:
			cleanup_field_content( f, body )
			if f['mandatory'] and len(f['content']) == 0:
				valid = False
		elif f['mandatory']:
			valid = False
	
	return valid, fields

def parse_submission_event( msg ):
	
	valid, fields = parse_form( msg, bbglob.FIELDS_EVENT )
	
	resp = MIMEMultipart()
	resp['From'] = hbb.SMTP_user
	resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
	resp['Subject'] = bbglob.SUBJECT_FLAG_EVENT
	
	if valid:
		
		resp['Subject'] += bbglob.SUBJECT_FLAG_VALIDATION + bbglob.SUBJECT_FLAG_END
		
		# saving fields in json
		with open( path_mail_fields( msg[ bbglob.BB_EMAIL_UNIQUE ] ), 'w') as f:
			json.dump( fields, f )
		
		# generating a preview
		preview = layouter.generate_preview( fields, bbglob.SUBJECT_FLAG_EVENT )
		with open( path_mail_preview( msg[ bbglob.BB_EMAIL_UNIQUE ] ), 'w' ) as f:
			f.write( preview )
			f.close()
		
		# pushing data in fields (fields var recycled!)
		fields = copy.deepcopy( bbglob.FIELDS_VALIDATION )
		for f in fields:
			if f['name'] == bbglob.SUBJECT_FLAG_UID:
				f['name'] += msg[ bbglob.BB_EMAIL_UNIQUE ]
				break
		
		txt = load_template( bbglob.TEMPLATE_PATH_EVENT_VALIDATION, fields )
		txt = txt.replace( '#PREVIEW#', preview )
		resp.attach( MIMEText( txt, 'plain' ) )
		
	else:
		
		resp['Subject'] += bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END
		resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_EVENT, fields, bbglob.MESSAGE_EVENT_SUB_ERROR ), 'plain' ) )
		
	send_msg( msg, resp, resp['Subject'] )

def parse_validation_event( msg ):
	
	valid, fields = parse_form( msg, bbglob.FIELDS_VALIDATION )
	
	uid =  ''
	stagingf = ''
	hotf = ''
	if valid:
		valid = False
		for f in fields:
			if f['name'] == bbglob.SUBJECT_FLAG_UID:
				# get the source folder
				uid = f['content']
				stagingf = path_mail_staging( uid )
				hotf = path_mail_hotfolder( uid )
				valid = bbglob.check_folder( stagingf )
				
	resp = MIMEMultipart()
	resp['From'] = hbb.SMTP_user
	resp['To'] = msg[ bbglob.BB_EMAIL_REPLY ]
	resp['Subject'] = bbglob.SUBJECT_FLAG_EVENT + bbglob.SUBJECT_FLAG_CONFIRM + bbglob.SUBJECT_FLAG_END
	
	if valid:
		
		# cool! the ref is good, we can publish!
		# loading data
		with open( path_mail_fields( uid ) ) as f:
			data = json.load(f)
			for d in data:
				if d['name'] == bbglob.SUBJECT_FLAG_EVENT_NAME:
					resp['Subject'] += ' "' + d['content'] + '"'
		
		# moving folder in hotfolder
		if bbglob.check_folder( hotf, False ):
			shutil.rmtree( hotf )
		shutil.copytree( stagingf, hotf )
		resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_MESSAGE, None, bbglob.MESSAGE_EVENT_VAL_OK ), 'plain' ) )
	
	else:
		
		resp.attach( MIMEText( load_template( bbglob.TEMPLATE_PATH_MESSAGE, None, bbglob.MESSAGE_EVENT_VAL_ERROR ), 'plain' ) )
		
	send_msg( msg, resp, resp['Subject'] )

def analyse_message( msg ):
	
	#import ipdb; ipdb.set_trace()
	
	# analysing subject
	subject = msg['content']['Subject']
	
	if msg[ bbglob.BB_EMAIL_ROLE ] != bbglob.Role.EDITOR and msg[ bbglob.BB_EMAIL_ROLE ] != bbglob.Role.ADMIN:
		bbglob.log( bbglob.LOG_MAIL, 'ERROR:: analyse_message, invalid access for [' + msg[ bbglob.BB_EMAIL_FROM ] + ']' )
		return
		
	if msg[ bbglob.BB_EMAIL_ROLE ] == bbglob.Role.ADMIN:
		if admin_message( msg ):
			return
	
	if subject != None:
		
		if subject.find( bbglob.SUBJECT_FLAG_EVENT ) != -1:
		
			if subject.find( bbglob.SUBJECT_FLAG_EVENT + bbglob.SUBJECT_FLAG_TEMPLATE + bbglob.SUBJECT_FLAG_END ) != -1:
				# it is an event template request!
				send_tmpl_message( msg, bbglob.SUBJECT_FLAG_EVENT )
			elif subject.find( bbglob.SUBJECT_FLAG_EVENT + bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END ) != -1:
				# it is an event submission!
				parse_submission_event( msg )
			elif subject.find( bbglob.SUBJECT_FLAG_EVENT + bbglob.SUBJECT_FLAG_VALIDATION + bbglob.SUBJECT_FLAG_END ) != -1:
				# it is an event validation!
				parse_validation_event( msg )
			return
		
		elif subject.find( bbglob.SUBJECT_FLAG_INFO ) != -1:
		
			if subject.find( bbglob.SUBJECT_FLAG_TEMPLATE + bbglob.SUBJECT_FLAG_END ) != -1:
				# it is an info template request!
				send_tmpl_message( msg, bbglob.SUBJECT_FLAG_INFO )
			elif subject.find( bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END ) != -1:
				print( 'it is an info submission!' )
			return
		
		elif subject.find( bbglob.SUBJECT_FLAG_NOTIF ) != -1:
		
			if subject.find( bbglob.SUBJECT_FLAG_TEMPLATE + bbglob.SUBJECT_FLAG_END ) != -1:
				# it is a notification template request!
				send_tmpl_message( msg, bbglob.SUBJECT_FLAG_NOTIF )
			elif subject.find( bbglob.SUBJECT_FLAG_SUBMISSION + bbglob.SUBJECT_FLAG_END ) != -1:
				print( 'it is an notification submission!' )
			return
	
	# no idea what this is... let send the welcoming email
	send_welcome_msg( msg )

def parse_mail( id, resp ):
	
	# import ipdb; ipdb.set_trace()
	
	if not 'Message-ID' in resp or not 'Return-Path' in resp:
		bbglob.log( bbglob.LOG_MAIL, 'ERROR:: no Message-ID or Return-Path in mail [' + str( id ) + ']' )
		return
	
	msg = {
		'content': resp
	}
	
	# unique ID of the message
	msg[ bbglob.BB_EMAIL_UNIQUE ] = str(id).replace( "'", '' ) + ':' + resp['Message-ID'].strip().replace('<','').replace('>','').replace('\r','').replace('\n','')
	
	# retrieval of sender
	if 'X-Sender' in resp:
		msg[ bbglob.BB_EMAIL_FROM ] = resp['X-Sender'].strip()
	elif 'From' in resp:
		
		emailaddr = get_email_address( resp['From'] )
		if emailaddr == None:
			bbglob.log( bbglob.LOG_MAIL, 'ERROR:: failed to locate email address [' + str( id ) + ']:' + msg[ bbglob.BB_EMAIL_UNIQUE ] )
		
		msg[ bbglob.BB_EMAIL_FROM ] = emailaddr
		
	else:
		
		bbglob.log( bbglob.LOG_MAIL, 'ERROR:: failed to locate email address [' + str( id ) + ']:' + msg[ bbglob.BB_EMAIL_UNIQUE ] )
		return
	
	# retrieval of reply address
	msg[ bbglob.BB_EMAIL_REPLY ] = resp['Return-Path'].strip().replace('<','').replace('>','')
	
	msg[ bbglob.BB_EMAIL_ROLE ] = get_access( msg )
	
	if not store_email( msg ):
		# already seen, no action required
		return
	
	# checking access rights
	
	if msg[ bbglob.BB_EMAIL_ROLE ] == bbglob.Role.ERROR:
		bbglob.log( bbglob.LOG_MAIL, 'ERROR:: parse_mail, invalid mail [' + msg[ bbglob.BB_EMAIL_UNIQUE ] + ']' )
		return
	
	elif msg[ bbglob.BB_EMAIL_ROLE ] == bbglob.Role.REJECT:
		bbglob.log( bbglob.LOG_MAIL, 'REJECT:: mail [' + str( msg[ bbglob.BB_EMAIL_UNIQUE ] ) + '] from ' + msg[ bbglob.BB_EMAIL_FROM ] )
		return
	
	elif msg[ bbglob.BB_EMAIL_ROLE ] == bbglob.Role.UNKNOWN:
		
		
		if bbglob.CONTROL_AUTO_EDITOR == True:
			r = autorise_user( msg[ bbglob.BB_EMAIL_FROM ] )
		else:
			send_registration_request( msg )
			return
	
	analyse_message( msg )

load_controls()

mail = imaplib.IMAP4_SSL( hbb.IMAP_server, hbb.IMAP_port )
mail.login( hbb.IMAP_user, hbb.IMAP_psswrd )
mail.select()

typ, data = mail.search( None, 'ALL' )

mail_ids = data[0]
id_list = mail_ids.split()

for id in id_list:
	typ, data = mail.fetch(id, '(RFC822)' )
	if typ != 'OK':
		continue
	for response_part in data:
		if isinstance(response_part, tuple) and len( response_part ) == 2:
			resp = None
			if isinstance( response_part[1], bytes ):
				resp = email.message_from_bytes( response_part[1] )
			elif isinstance( response_part[1], str ):
				print( 'getting string' )
				resp = email.message_from_string( response_part[1] )
			if resp != None:
				parse_mail( id, resp )

'''
IMAP4.recent()
Prompt server for an update. Returned data is None if no new messages, else value of RECENT response.
'''

mail.close()
mail.logout()