import os
import pyfiglet
import termcolor
import time

import imager
import tabler

output = ''

########### STYLES & FORMATING ###########

rows, cols = os.popen('stty size', 'r').read().split()
rows = int( rows )
cols = 80

base = tabler.tabler_config()
base['canvas'][0] = 80
base['display_rect'][2] = 80
base['cell_padding'] = [0,0,0,0]
base['cell_border'] = 0
base['table_columns'] = 1
base['dump'] = False
base['cell_style_enabled'] = False
base['word_wrap'] = 'word'

h1 = tabler.tabler_config( base )
h1['border_char'] = '#'
h1['vertical_align'] = 'center'
h1['cell_border'] = 1
h1_font = pyfiglet.Figlet(font='speed', width=cols)

h2 = tabler.tabler_config( base )
h2['border_char'] = '/'
h2['vertical_align'] = 'center'
h2['cell_border'] = 1
h2_font = pyfiglet.Figlet(font='contessa', width=cols)

p = tabler.tabler_config( base )
p['vertical_align'] = 'left'
p['cell_border'] = 0
p['padding_char'] = ' '
p['cell_padding'] = [1,0,1,0]

tfiles = tabler.tabler_config( base )
tfiles['table_columns'] = 3
tfiles['columns_width'] = [0.23, 0.7, 0.12]
tfiles['cell_border'] = 0
tfiles['cell_padding'] = [0,1,1,0]
tfiles['packing'] = 'strict_H'

tdoc = tabler.tabler_config( base )
tdoc['table_columns'] = 2
tdoc['columns_width'] = [0.28, 0.8]
tdoc['cell_border'] = 0
tdoc['cell_padding'] = [0,1,1,0]
tdoc['packing'] = 'strict_H'
tdoc['canvas'][1] = 1000
tdoc['display_rect'][3] = tdoc['canvas'][1]

demo = tabler.tabler_config( base )
demo['table_columns'] = 3
demo['cell_border'] = 1
demo['cell_padding'] = [1,1,1,1]

########### CONTENT ###########

txt = h1_font.renderText('Hacktiris Billboard')
output += tabler.quick_table( txt, h1 )

txt = 'python suite to create billboards with multi pages, timer, animation and image transformation into ascii displayable in terminal'
output += tabler.quick_table( txt, p )

# requirements

txt = h2_font.renderText('requirements')
output += tabler.quick_table( txt, h2 )

p['vertical_align'] = 'left'

txt = '''/ python:

\t$ pip install pyfiglet
\t$ pip install termcolor
\t$ pip install Pillow
\t$ pip install numpy
\t$ pip install dateparser

/ third-party:

\t$ sudo apt install ffmpeg
'''
output += tabler.quick_table( txt, p )

# usage

txt = h2_font.renderText('usage')
output += tabler.quick_table( txt, h2 )

output += tabler.quick_table( "/ python files in this repository", p )

txt = [
	"*file*",
	{ 'cfg': { 'vertical_align': 'center' } ,'content':"*description*" },
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"*status*" }, 
	"gifer.py", "convert animated gifs into text sequence, requires ffmpeg & imager.py", 
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"ready" },
	"imager.py", "convert a single image into text", 
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"ready" },
	"mailer.py", "mail server, receives and sends mails to update the billboard", 
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"ready" },
	"layouter.py", "turns mail content into displayable text", 
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"in dev" },
	"pager.py", "management of pages",  
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"in dev" },
	"pyfligeter.py", "print all fonts avalable in filget, sorted by font name",  
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"ready" },
	"readme.py", "used to generate this readme",  
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"ready" },
	"tabler.py", "generation of tables and management of styles",  
	{ 'cfg': { 'vertical_align': 'right' } ,'content':"ready" }
]
output += tabler.tabler( txt, tfiles )

output += tabler.quick_table( "/ how to use tabler.py?", p )
txt = [
	"tabler_config(c)","generates a standard configuration of a table, with styles enabled and 4 columns\nreturns a deepcopy of the passed configuration if some",
	"quick_table(t,c)","generates a table with one column\n- arg[0]: text to display\n- arg[1]: configuration",
	"tabler(t,c)","generates a table with multiple columns and rows\n- arg[0]: list of cells/text to display\n- arg[1]: configuration"
]
output += tabler.tabler( txt, tdoc )

output += tabler.quick_table( "/ table configuration", p )
txt = [
	"*general","",
	"canvas","[ width, height ], the canvas to draw the table, set automatically to the terminal window dimensions\nunit: character",
	"display_rect","[ x, y width, height ] visible part of the canvas, set automatically to [ 0, 0, canvas[0], canvas[1] ]\nunit: character",
	"dump","if true, print the table directly in the terminal",
	"export_path","path of a file to dump the table content, does not create folders",
	"packing","describes the way to put the lines in the table\n- strict_H: horizontal packing, rows are created first and all cells from the same row have the same height\n- strict_V: vertical packing, columns are created first then stacked and all cells from the same row have the same height\n- fluid_H: horizontal packing, rows are created first and cells placed in the next smaller column\n- fluid_V: vertical packing, columns are created first and avoid reaching the canvas height, might generates new columns\nsee below for packing demo",
	"table_columns","number of columns of the table\nstrictly applied by all packing except 'fluid_V'",
	"return","type of return of tabler() function\n- cropped: content ready to print\n- lines: each lines of the table, not cropped with display_rect\n- cells: all cells of the table\n- size: disable content processing, just compute the dimensions of the table, to be retrieved directly in the config (usefull when using nested tables)\n- none: nothing",
	"word_wrap","word wrapping method\n- 'letter': by default, line returns anywhere in the the text, ensure an optimised cell size\n- 'word', tries to only generate line returns between word\n- 'none': truncates long lines (usefull for images!)",
	"*styling","",
	"border_char","character to be used for borders, '·' by default",
	"border_color","decoration of the border, blank by default",
	"cell_border","thickness of the borders",
	"cell_maxh","maximum cell height, set to -1 to disable",
	"cell_padding","[ top, right, bottom, left ] padding of each cell",
	"cell_style_enabled","if true, special characters will be added to set decoration",
	"cell_style_even","decoration of the even cells, '\\033[40m\\033[1;37m' by default, white text on black bg",
	"cell_style_odd","decoration of the odd cells, '\\033[47m\\033[1;31m' by default, red text on grey bg",
	"empty_char","character to be used for empty spaces, ' ' by default",
	"padding_char","character to be used for padding, ' ' by default",
	"truncated_marker","when cell_maxh is enabled and content is too long, this marker will be appended at the end of the text, '[...]' by default",
	"vertical_align","vertical alignment of text in cell, 'left', 'center' or 'right'",
	"style_reset","command used to reset the default decoration, '\\033[m' by default",
	"*processed","",
	"table_width","width of the table without the borders, automatically computed",
	"cell_outer","width of each column, including padding but without borders",
	"cell_offset","horizontal offset of each column, without borders",
]
output += tabler.tabler( txt, tdoc )

output += tabler.quick_table( "/ cell configuration", p )
txt = [
	"*general","",
	"word_wrap","overwrites the table configuration, 'letter' (default), 'word' or 'none'",
	"*styling","",
	"cell_padding","[ top, right, bottom, left ] padding of this cell",
	"cell_maxh","maximum cell height, set to -1 to disable",
	"vertical_align","vertical alignment of text in cell, 'left', 'center' or 'right'",
	"*processed","",
	"cell_inner","width of the cell without borders nor padding",
]
output += tabler.tabler( txt, tdoc )

output += tabler.quick_table( "/ packing demo : strict_H", p )
txt = [
	"[1] Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum et consectetur massa, placerat pretium turpis",
	"[2] python suite to create billboards with multi pages, timer, animation and image transformation into ascii",
	"[3] https://twoglasshams.itch.io/ashi-wash-classic",
	"[4] render form each side in png, orthogonal projection relocate the origin",
	"[5]\n\t- https://robotvault.bitbucket.io/scenenet-rgbd.html\n\t- https://carlburton.itch.io/islands"
]
output += tabler.tabler( txt, demo )

output += tabler.quick_table( "/ packing demo : strict_V", p )
demo['packing'] = 'strict_V'
output += tabler.tabler( txt, demo )

output += tabler.quick_table( "/ packing demo : fluid_H", p )
demo['packing'] = 'fluid_H'
output += tabler.tabler( txt, demo )

output += tabler.quick_table( "/ packing demo : fluid_V", p )
demo['packing'] = 'fluid_V'
demo['canvas'][1] = 20
output += tabler.tabler( txt, demo )

# resources

txt = h2_font.renderText('resources')
output += tabler.quick_table( txt, h2 )

txt = '''
image manipulation

- image to ascii: https://www.geeksforgeeks.org/converting-image-ascii-image-python/
- image resize: https://duckduckgo.com/?q=python+pil+resize+image&t=lm&ia=web&iax=qa

gif resource:

- goat/0001:
https://i.pinimg.com/originals/be/93/99/be93997cc655c66b88d9c2ad50a1d658.gif
'''
output += tabler.quick_table( txt, p )

f = open( 'README.md', 'w' )
f.write( '```bash\n' + output + '```' )